package com.pmu.entity;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

class PartantTest {
    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link Partant#setCourse(Course)}
     *   <li>{@link Partant#setName(String)}
     *   <li>{@link Partant#setPartantId(Long)}
     *   <li>{@link Partant#setPartantNumber(int)}
     *   <li>{@link Partant#toString()}
     *   <li>{@link Partant#getCourse()}
     *   <li>{@link Partant#getName()}
     *   <li>{@link Partant#getPartantId()}
     *   <li>{@link Partant#getPartantNumber()}
     * </ul>
     */
    @Test
    void testSetCourse() {
        Partant partant = new Partant();

        Course course = new Course();
        course.setCourse(1L);
        course.setCourseDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        course.setCourseNumber(10);
        course.setName("Name");
        course.setPartants(new HashSet<>());
        partant.setCourse(course);
        partant.setName("Name");
        partant.setPartantId(1L);
        partant.setPartantNumber(10);
        String actualToStringResult = partant.toString();
        Course actualCourse = partant.getCourse();
        String actualName = partant.getName();
        Long actualPartantId = partant.getPartantId();
        int actualPartantNumber = partant.getPartantNumber();
        assertSame(course, actualCourse);
        assertEquals("Name", actualName);
        assertEquals(1L, actualPartantId.longValue());
        assertEquals(10, actualPartantNumber);
        assertEquals("Partant(partantId=1, name=Name, partantNumber=10, course=Course(course=1, name=Name, courseNumber=10,"
                + " courseDate=1970-01-01T00:00, partants=[]))", actualToStringResult);
    }
}

