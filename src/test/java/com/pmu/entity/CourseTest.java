package com.pmu.entity;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertSame;

class CourseTest {
    /**
     * Method under test: {@link Course#setPartants(Set)}
     */
    @Test
    void testSetPartants() {
        Course course = new Course();
        HashSet<Partant> partants = new HashSet<>();
        course.setPartants(partants);
        assertSame(partants, course.getPartants());
    }

    /**
     * Method under test: {@link Course#setPartants(Set)}
     */
    @Test
    void testSetPartants2() {
        Course course = new Course();

        Course course2 = new Course();
        course2.setCourse(1L);
        course2.setCourseDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        course2.setCourseNumber(10);
        course2.setName("Name");
        course2.setPartants(new HashSet<>());

        Partant partant = new Partant();
        partant.setCourse(course2);
        partant.setName("Name");
        partant.setPartantId(1L);
        partant.setPartantNumber(10);

        HashSet<Partant> partants = new HashSet<>();
        partants.add(partant);
        course.setPartants(partants);
        assertSame(partants, course.getPartants());
    }

    /**
     * Method under test: {@link Course#setPartants(Set)}
     */
    @Test
    void testSetPartants3() {
        Course course = new Course();

        Course course2 = new Course();
        course2.setCourse(1L);
        course2.setCourseDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        course2.setCourseNumber(10);
        course2.setName("Name");
        course2.setPartants(new HashSet<>());

        Partant partant = new Partant();
        partant.setCourse(course2);
        partant.setName("Name");
        partant.setPartantId(1L);
        partant.setPartantNumber(10);

        Course course3 = new Course();
        course3.setCourse(0L);
        course3.setCourseDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        course3.setCourseNumber(1);
        course3.setName("42");
        course3.setPartants(new HashSet<>());

        Partant partant2 = new Partant();
        partant2.setCourse(course3);
        partant2.setName("42");
        partant2.setPartantId(2L);
        partant2.setPartantNumber(1);

        HashSet<Partant> partants = new HashSet<>();
        partants.add(partant2);
        partants.add(partant);
        course.setPartants(partants);
        assertSame(partants, course.getPartants());
    }
}

