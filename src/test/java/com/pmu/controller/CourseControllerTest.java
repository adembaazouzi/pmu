package com.pmu.controller;

import com.pmu.entity.Course;
import com.pmu.entity.Partant;
import com.pmu.exception.InvalidDataException;
import com.pmu.repository.CourseRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class CourseControllerTest {
    /**
     * Method under test: {@link CourseController#createCourse(Course)}
     */
    @Test
    void testCreateCourse() {

        CourseController courseController = new CourseController(mock(CourseRepository.class));

        Course course = new Course();
        course.setCourse(1L);
        course.setCourseDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        course.setCourseNumber(10);
        course.setName("Name");
        course.setPartants(new HashSet<>());
        assertThrows(InvalidDataException.class, () -> courseController.createCourse(course));
    }

    /**
     * Method under test: {@link CourseController#createCourse(Course)}
     */
    @Test
    void testCreateCourse2() {

        CourseController courseController = new CourseController(mock(CourseRepository.class));
        Course course = mock(Course.class);
        when(course.getName()).thenReturn("Name");
        when(course.getPartants()).thenReturn(new HashSet<>());
        doNothing().when(course).setCourse(Mockito.<Long>any());
        doNothing().when(course).setCourseDate(Mockito.<LocalDateTime>any());
        doNothing().when(course).setCourseNumber(anyInt());
        doNothing().when(course).setName(Mockito.<String>any());
        doNothing().when(course).setPartants(Mockito.<Set<Partant>>any());
        course.setCourse(1L);
        course.setCourseDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        course.setCourseNumber(10);
        course.setName("Name");
        course.setPartants(new HashSet<>());
        assertThrows(InvalidDataException.class, () -> courseController.createCourse(course));
        verify(course, atLeast(1)).getName();
        verify(course, atLeast(1)).getPartants();
        verify(course).setCourse(Mockito.<Long>any());
        verify(course).setCourseDate(Mockito.<LocalDateTime>any());
        verify(course).setCourseNumber(anyInt());
        verify(course).setName(Mockito.<String>any());
        verify(course).setPartants(Mockito.<Set<Partant>>any());
    }
}

