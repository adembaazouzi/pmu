DROP TABLE IF EXISTS partant;
DROP TABLE IF EXISTS course;

CREATE TABLE `course`
(
    `course_id`     BIGINT(50) AUTO_INCREMENT PRIMARY KEY,
    `course_name`   varchar(50) NOT NULL,
    `course_number` int         NOT NULL,
    `course_date`   timestamp   NOT NULL,
    CONSTRAINT UC_course UNIQUE (course_name, course_number, course_date)
);


CREATE TABLE `partant`
(
    `partant_id`     BIGINT(50) AUTO_INCREMENT PRIMARY KEY,
    `partant_number` int         NOT NULL,
    `partant_name`   varchar(50) NOT NULL,
    course           BIGINT(50) NOT NULL,
    FOREIGN KEY (`course`) REFERENCES `course` (`course_id`) ON DELETE CASCADE
);