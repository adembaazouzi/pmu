package com.pmu.exception;

import com.pmu.validation.ValidationReason;
import com.pmu.validation.ValidationResult;

import java.util.Set;

public class CourseValidationException extends PmuException {
    private final ValidationResult validationResult;

    public CourseValidationException(String message, ValidationResult validationResult) {
        super(message);
        this.validationResult = validationResult;
    }

    public CourseValidationException(ValidationResult validationResult) {
        this(validationResult.toString(), validationResult);
    }

    public CourseValidationException(String message) {
        this(message, ValidationResult.invalid());
    }

    public ValidationResult getValidationResult() {
        return this.validationResult;
    }

    public boolean isValid() {
        return this.validationResult.isValid();
    }

    public Set<ValidationReason> getReasons() {
        return this.validationResult.getReasons();
    }

    public String toString() {
        return "CourseValidationException(message=" + this.getMessage() + ", validationResult=" + this.getValidationResult() + ")";
    }

}
