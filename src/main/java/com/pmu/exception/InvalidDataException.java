package com.pmu.exception;


import com.pmu.validation.ValidationResult;

public class InvalidDataException extends CourseValidationException {
    public static final String DEFAULT_MESSAGE = "Error course data";


    public InvalidDataException(ValidationResult validationResult) {
        super(DEFAULT_MESSAGE, validationResult);
    }

    public InvalidDataException(String message) {
        super(message, ValidationResult.invalid());
    }

    public InvalidDataException(String message, ValidationResult validationResult) {
        super(message, validationResult);
    }
}
