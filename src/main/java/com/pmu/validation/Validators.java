package com.pmu.validation;


import java.util.Objects;

public final class Validators {
    public static FunctionValidator<Object> nullParameter = FunctionValidator.createValidator(Objects::isNull, ValidationReason.ERROR_PARAM);
    public static FunctionValidator<String> emptyStringParameter = FunctionValidator.createValidator(String::isEmpty, ValidationReason.ERROR_PARAM);
    public static FunctionValidator<String> blankStringParameter = FunctionValidator.createValidator(String::isBlank, ValidationReason.ERROR_PARAM);
}
