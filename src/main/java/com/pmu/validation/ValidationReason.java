package com.pmu.validation;

public enum ValidationReason {
    ERROR_PARAM,
    EMPTY_COURSE;

    public String toString() {
        return name();
    }

    public static ValidationReason fromString(String reason) {
        return ValidationReason.valueOf(reason);
    }


}
