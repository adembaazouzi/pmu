package com.pmu.validation;


import com.pmu.entity.Course;

import java.util.function.Predicate;


public interface CourseValidationPredicate extends Predicate<Course> {
    CourseValidationPredicate isEmptyRuleName = (course) -> course.getName() == null || course.getName().isEmpty();
    CourseValidationPredicate isEmptyRuleCourseNumber = (course) -> course.getCourseNumber() == 0;
    CourseValidationPredicate isEmptyRuleCourseDate = (course) -> course.getCourseDate() == null;
    CourseValidationPredicate isEmptyRulePartants = (course) -> course.getPartants() == null || course.getPartants().isEmpty();
    CourseValidationPredicate isEmptyRuleCourse = (course) -> course.getCourse() == null;
    CourseValidationPredicate coursePartantsSizeLowThanThree = (course) -> course.getPartants() == null || course.getPartants().size() < 3;

}
