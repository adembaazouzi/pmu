package com.pmu.validation;


import com.pmu.entity.Course;

public final class CourseValidators {
    public static FunctionValidator<Course> emptyName = FunctionValidator.createValidator(CourseValidationPredicate.isEmptyRuleName, ValidationReason.EMPTY_COURSE);
    public static FunctionValidator<Course> coursePartantsSizeLowThanThree = FunctionValidator.createValidator(CourseValidationPredicate.coursePartantsSizeLowThanThree, ValidationReason.EMPTY_COURSE);

    private CourseValidators() {
    }

    public static FunctionValidator<Course> emptyCourseNumber = FunctionValidator.createValidator(CourseValidationPredicate.isEmptyRuleCourseNumber, ValidationReason.EMPTY_COURSE);

    public static FunctionValidator<Course> emptyCourseDate = FunctionValidator.createValidator(CourseValidationPredicate.isEmptyRuleCourseDate, ValidationReason.EMPTY_COURSE);

    public static FunctionValidator<Course> emptyPartants = FunctionValidator.createValidator(CourseValidationPredicate.isEmptyRulePartants, ValidationReason.EMPTY_COURSE);

    public static FunctionValidator<Course> emptyCourse = FunctionValidator.createValidator(CourseValidationPredicate.isEmptyRuleCourse, ValidationReason.EMPTY_COURSE);

}
