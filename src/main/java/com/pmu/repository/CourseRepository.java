package com.pmu.repository;

import com.pmu.entity.Course;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.CrudRepository;

import java.awt.print.Pageable;
import java.time.LocalDateTime;
import java.util.List;

public interface CourseRepository extends CrudRepository<Course, Long> {

    Course findByName(String name);

    Course findByCourseNumber(int courseNumber);

    Course findByCourseDate(LocalDateTime courseDate);

    List<Course> findByNameLike(String name);

    List<Course> findAll();

    Page<Course> findAll(Pageable pageable);

}
