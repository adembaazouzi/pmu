package com.pmu.repository;

import com.pmu.entity.Course;
import com.pmu.entity.Partant;
import org.springframework.data.repository.CrudRepository;

public interface PartantRepository extends CrudRepository<Partant, Long> {
    Partant findByName(String name);

    Partant findByPartantNumber(int partantNumber);

    Partant findByCourse(Course course);


}
