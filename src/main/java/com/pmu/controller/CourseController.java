package com.pmu.controller;

import com.pmu.entity.Course;
import com.pmu.exception.InvalidDataException;
import com.pmu.repository.CourseRepository;
import com.pmu.validation.CourseValidators;
import com.pmu.validation.FunctionValidator;
import com.pmu.validation.ValidationResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class CourseController {

    private final CourseRepository courseRepository;

    public CourseController(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @PostMapping
    public Course createCourse(@RequestBody Course course) {

        FunctionValidator<Course> validator = CourseValidators.emptyName
                .and(CourseValidators.coursePartantsSizeLowThanThree);
        ValidationResult validationResult = validator.apply(course);

        if (!validationResult.isValid()) {
            throw new InvalidDataException(validationResult);
        }

        return courseRepository.save(course);
    }


}
