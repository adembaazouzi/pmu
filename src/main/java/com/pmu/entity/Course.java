package com.pmu.entity;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Course implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "course_id")
    private Long course;
    @Column(name = "course_name")
    private String name;
    @Column(name = "course_number")
    private int courseNumber;
    @Column(name = "course_date")
    private LocalDateTime courseDate;
    @OneToMany(mappedBy = "course", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)

    private Set<Partant> partants = new HashSet<>();

    public void setPartants(Set<Partant> partants) {
        this.partants = partants;
        partants.stream().forEach(p -> p.setCourse(this));
    }

    public Long getCourse() {
        return this.course;
    }

    public String getName() {
        return this.name;
    }

    public int getCourseNumber() {
        return this.courseNumber;
    }

    public LocalDateTime getCourseDate() {
        return this.courseDate;
    }

    public Set<Partant> getPartants() {
        return this.partants;
    }

    public void setCourse(Long course) {
        this.course = course;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCourseNumber(int courseNumber) {
        this.courseNumber = courseNumber;
    }

    public void setCourseDate(LocalDateTime courseDate) {
        this.courseDate = courseDate;
    }

    public String toString() {
        return "Course(course=" + this.getCourse() + ", name=" + this.getName() + ", courseNumber=" + this.getCourseNumber() + ", courseDate=" + this.getCourseDate() + ", partants=" + this.getPartants() + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return course.getCourse() == this.getCourse();
    }

    @Override
    public int hashCode() {
        return (int) (this.getCourse() ^ (this.getCourse() >>> 32));
    }
}
