package com.pmu.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
public class Partant implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "partant_id")
    private Long partantId;
    @Column(name = "partant_name")
    private String name;
    @Column(name = "partant_number")
    private int partantNumber;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "course", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Course course;


    public Long getPartantId() {
        return this.partantId;
    }

    public String getName() {
        return this.name;
    }

    public int getPartantNumber() {
        return this.partantNumber;
    }

    public Course getCourse() {
        return this.course;
    }

    public void setPartantId(Long partantId) {
        this.partantId = partantId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPartantNumber(int partantNumber) {
        this.partantNumber = partantNumber;
    }

    @JsonIgnore
    public void setCourse(Course course) {
        this.course = course;
    }

    public String toString() {
        return "Partant(partantId=" + this.getPartantId() + ", name=" + this.getName() + ", partantNumber=" + this.getPartantNumber() + ", course=" + this.getCourse() + ")";
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Partant partant = (Partant) o;
        return this.getPartantId() == partant.getPartantId();
    }
}
